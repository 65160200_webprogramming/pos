import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User>({
    id: 1,
    email: '65160200@go.buu.ac.th',
    password: 'pass1234',
    fullName: 'Sitipog Wongchareon',
    gender: 'male',
    roles: ['user']
  })

  return { currentUser }
})
