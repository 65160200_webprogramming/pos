import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'สิทธิพงษ์ วงศ์เจริญ', tel: '0999999999' },
    { id: 2, name: 'สุทธิพงษ์ วงศ์เจริญ', tel: '0123456789' }
  ])
  const currentMember = ref<Member|null>()
  function searchMember(tel: string) {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
        currentMember.value = null
    }
    return currentMember.value = members.value[index]
  }

function clear() {
  currentMember.value = null
}

  return {
    members, currentMember,
    searchMember, clear
  }
})
